package com.example.activitystatechange

import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivity : AppCompatActivity() {
    var media: MediaPlayer? = null;
    val KEY_STATE: String = "media";

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        if(savedInstanceState != null && savedInstanceState.containsKey(KEY_STATE)){
            media?.seekTo(savedInstanceState.getInt(KEY_STATE,0));
        }
        Log.i("LOG","onRestoreInstanceState");

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.i("LOG","onCreate")

        if(media == null)
            createMedia();

    }

    fun createMedia(){
        media = MediaPlayer.create(this,R.raw.bynd);
        media?.start()
    }

    override fun onPause() {
        super.onPause()
        media?.pause();
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        media?.currentPosition?.let { outState.putInt(KEY_STATE, it) };

        Log.i("LOG","onSaveInstanceState")

    }



}