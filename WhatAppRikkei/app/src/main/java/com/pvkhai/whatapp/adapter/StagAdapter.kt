package com.pvkhai.whatapp.adapter

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.pvkhai.whatapp.R
import com.pvkhai.whatapp.model.UserInfo
import com.pvkhai.whatapp.service.FirebaseService
import kotlinx.android.synthetic.main.fragment_chat.view.*

class StagAdapter(
    val data: Array<String>,
    val context: Context,
    val userInfo: UserInfo
) : RecyclerView.Adapter<StagAdapter.PlaceHolder>() {
    val firebase = FirebaseService()

    class PlaceHolder(var view: View) : RecyclerView.ViewHolder(view) {
        val image: ImageView = view.findViewById(R.id.img)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaceHolder {

        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.item_sticker, parent, false);
        return PlaceHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: PlaceHolder, position: Int) {
        holder.image.setImageResource(
            context.resources.getIdentifier(
                data[position],
                "mipmap",
                context.packageName
            )
        );
        holder.image.setOnClickListener {
            sendMessage(data[position]);
        }
    }

    private fun sendMessage(sticker: String) {
        val tsLong = System.currentTimeMillis()

        var messageSendRef = "Messages/" + firebase.getID() + "/" + userInfo.uid;
        var messageReceivedRef = "Messages/" + userInfo.uid + "/" + firebase.getID();

        var userMessageKeyRef =
            firebase.messagesRef.child(firebase.getID()).child(userInfo.uid).push();

        var messageID = userMessageKeyRef.key;

        var messageBody = HashMap<String, Any>();
        messageBody["message"] = sticker;
        messageBody["type"] = "sticker";
        messageBody["form"] = firebase.getID();
        messageBody["time"] = tsLong.toString();
        messageBody["isshow"] = true;
        messageBody["checkseen"] = false;

        var messageDetail = HashMap<String, Any>();
        messageDetail["$messageSendRef/$messageID"] = messageBody;
        messageDetail["$messageReceivedRef/$messageID"] = messageBody;

        firebase.rootRef.updateChildren(messageDetail).addOnCompleteListener {
            if (it.isSuccessful) {
                Toast.makeText(context, "oke oke", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
