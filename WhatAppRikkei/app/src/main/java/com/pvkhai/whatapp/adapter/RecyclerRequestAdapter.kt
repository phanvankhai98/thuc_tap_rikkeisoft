package com.pvkhai.whatapp.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.pvkhai.whatapp.R
import com.pvkhai.whatapp.model.UserInfo
import com.pvkhai.whatapp.service.FirebaseService
import com.squareup.picasso.Picasso


class RecyclerRequestAdapter(val data: ArrayList<UserInfo>, val context: Context) :
    RecyclerView.Adapter<RecyclerAllAdapter.FriendViewHolder>() {
    val firebase: FirebaseService = FirebaseService();
    val ACCEPT: Int = 1;
    val CANCEL: Int = 0;
    var requestType = "Received"


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerAllAdapter.FriendViewHolder {
        val contactView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_friend, parent, false);
        return RecyclerAllAdapter.FriendViewHolder(contactView);
    }

    //    override fun onBindViewHolder(holder: FriendViewHolder, position: Int, model: UserInfo) {
//
//    }
    override fun onBindViewHolder(holder: RecyclerAllAdapter.FriendViewHolder, position: Int) {

        var user = data[position]
        managerRequest(user, holder);

        if (position == 0) {
            if (data[position].request == "Received") {
                holder.tvSession.visibility = View.VISIBLE;
                holder.tvSession.text = "Lời mời kết bạn".toUpperCase();
            } else {
                holder.tvSession.visibility = View.VISIBLE;
                holder.tvSession.text = "Đã gửi kết bạn".toUpperCase();
            }
        } else {
            if (data[position].request == "Received")
                holder.tvSession.visibility = View.GONE;
            else if (data[position - 1].request != "Sent") {
                holder.tvSession.visibility = View.VISIBLE;
                holder.tvSession.text = "Đã gửi kết bạn".toUpperCase();
            } else
                holder.tvSession.visibility = View.GONE;

        }


        holder.tvName.text = user.name;
        Picasso.get()
            .load(user.image)
            .placeholder(R.mipmap.loading)
            .into(holder.imgAvatar);
        holder.tvStatus.setOnClickListener {
            when (holder.tvStatus.text) {
                context.getString(R.string.add_friend) -> changeStatusFriend(holder, user, "Sent");
                context.getString(R.string.cancel) -> removeRequestFriend(holder, user, CANCEL);
                context.getString(R.string.confirm_add) -> changeStatusFriend(
                    holder,
                    user,
                    "Saved"
                );
            }
        }

    }

    override fun getItemCount(): Int {
        return data.size;
    }

    private fun changeStatusFriend(
        holder: RecyclerAllAdapter.FriendViewHolder,
        model: UserInfo,
        status: String
    ) {

        var receivedStatus = "Received"
        if (status == "Saved") receivedStatus = "Saved";

        firebase.contactsRef.child(firebase.getID()).child(model.uid).child("Contacts")
            .setValue(status).addOnCompleteListener {
                if (it.isSuccessful) {
                    firebase.contactsRef.child(model.uid).child(firebase.getID()).child("Contacts")
                        .setValue(receivedStatus).addOnCompleteListener {
//                            removeRequestFriend(holder, model, ACCEPT);
                            setStatus(holder, context.getString(R.string.cancel))
                        }
                }
            }
    }


    private fun removeRequestFriend(holder: RecyclerAllAdapter.FriendViewHolder, model: UserInfo, type: Int) {
        firebase.contactsRef.child(firebase.getID()).child(model.uid).removeValue()
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    firebase.contactsRef.child(model.uid).child(firebase.getID()).removeValue()
                        .addOnCompleteListener {
//                            if (type == CANCEL)
                            setStatus(holder, context.getString(R.string.add_friend))
//                            else
//                                holder.tvStatus.isVisible = false;
                        }
                }
            }
    }

    private fun managerRequest(user: UserInfo, holder: RecyclerAllAdapter.FriendViewHolder) {
        if (firebase.getID() == user.uid) {
            holder.tvStatus.visibility = View.INVISIBLE
        } else {
            holder.tvStatus.visibility = View.VISIBLE
        }

        when (user.request) {
            "Sent" -> {
                holder.tvStatus.visibility = View.VISIBLE
                setStatus(holder, context.getString(R.string.cancel))
            }
            "Received" -> {
                holder.tvStatus.visibility = View.VISIBLE
                setStatus(holder, context.getString(R.string.confirm_add))
            }
            "Saved" -> holder.tvStatus.visibility = View.INVISIBLE;
            else -> setStatus(holder, context.getString(R.string.add_friend))
//            else -> {
//                if (user.contact) {
//                    holder.tvStatus.isVisible = false;
//                } else {
//                    setStatus(
//                        holder, context.getString(R.string.add_friend)
//                    )
//                }
//            }
        }


    }

    fun setStatus(holder: RecyclerAllAdapter.FriendViewHolder, status: String) {
        when (status) {
            context.getString(R.string.cancel) -> {
                holder.tvStatus.text = context.getString(R.string.cancel);
                holder.tvStatus.setBackgroundResource(R.drawable.custom_delete_friend);
                holder.tvStatus.setTextColor(context.resources.getColor(R.color.colorPrimary));
            };
            context.getString(R.string.add_friend) -> {
                holder.tvStatus.text = context.getString(R.string.add_friend);
                holder.tvStatus.setBackgroundResource(R.drawable.custom_add_friend);
                holder.tvStatus.setTextColor(context.resources.getColor(R.color.colorWhite));
            }
            context.getString(R.string.confirm_add) -> {
                holder.tvStatus.text = context.getString(R.string.confirm_add);
                holder.tvStatus.setBackgroundResource(R.drawable.custom_add_friend);
                holder.tvStatus.setTextColor(context.resources.getColor(R.color.colorWhite));
            }
        }
    }



}