package com.pvkhai.whatapp.util

import java.text.SimpleDateFormat
import java.util.*
var DATE_FORMAT_1 = "hh:mm a"
var DATE_FORMAT_2 = "dd/MM/yyyy"

class FuntionUtil {
    private constructor() {
    }
    companion object{
        fun convertTimestampToTime(timestamp: String):String{
            var formatter =  SimpleDateFormat(DATE_FORMAT_1);
            var dateString =  Date(timestamp.toLong())
            return formatter.format(dateString);
        }
        fun convertTimestampToDate(timestamp: String):String{
            var formatter =  SimpleDateFormat(DATE_FORMAT_2);
            var dateString =  Date(timestamp.toLong())
            return formatter.format(dateString);
        }
        fun getTime( timeStr: String): String{
            val tsLong = System.currentTimeMillis()
            var time = ""
            val differenceTime = tsLong - timeStr.toLong()
            if (differenceTime <= 86400000)
                time = convertTimestampToTime(timeStr)
            else if (differenceTime < 86400000 * 2)
                time = "Hôm qua"
            else
                time = convertTimestampToDate(timeStr);

            return time;
        }

    }

}