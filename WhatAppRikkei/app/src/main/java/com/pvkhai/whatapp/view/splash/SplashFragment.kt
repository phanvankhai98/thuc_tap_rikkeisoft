package com.pvkhai.whatapp.view.splash

import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.pvkhai.whatapp.R
import com.pvkhai.whatapp.service.FirebaseService

class SplashFragment : Fragment() {


    private var currentUser: FirebaseUser? = null;
    private var mAuth: FirebaseAuth? = null;
    var rootRef = FirebaseDatabase.getInstance().reference;

    private fun sendUserToHome() {

        this.findNavController().navigate(R.id.action_splashFragment_to_homeFragment)
//        var currentUserID: String = firebase.getUser()?.uid!!;
//        val postListener = object : ValueEventListener {
//            override fun onCancelled(error: DatabaseError) {
//
//            }
//            override fun onDataChange(snapshot: DataSnapshot) {
//                if (snapshot.child("name").exists()) {
//                    view?.findNavController()
//                        ?.navigate(R.id.action_splashFragment_to_homeFragment);
//                    Toast.makeText(activity, "Wellcome", Toast.LENGTH_SHORT).show();
//                } else
//                    view?.findNavController()
//                        ?.navigate(R.id.action_splashFragment_to_userInfoFragment);
//            }
//        }
//        rootRef.child(currentUserID).addValueEventListener(postListener)
    }

//    override fun onStart() {
//        super.onStart()
//        var firebase = FirebaseService();
//        var currentUserID: String = firebase.getUser()?.uid!!;
//        val postListener = object : ValueEventListener {
//            override fun onCancelled(error: DatabaseError) {
//
//            }
//            override fun onDataChange(snapshot: DataSnapshot) {
//                if (snapshot.child("name").exists()) {
//                    view?.findNavController()
//                        ?.navigate(R.id.action_splashFragment_to_homeFragment);
//                    Toast.makeText(activity, "Wellcome", Toast.LENGTH_SHORT).show();
//                } else
//                    view?.findNavController()
//                        ?.navigate(R.id.action_splashFragment_to_userInfoFragment);
//            }
//        }
//        rootRef.child(currentUserID).addValueEventListener(postListener)
//    }

    private fun sendUserToLogin() {
        this.findNavController().navigate(R.id.action_splashFragment_to_loginFragment)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var firebase = FirebaseService();
        currentUser = firebase.getUser()
        var view: View = inflater.inflate(R.layout.fragment_splash, container, false);
        Handler().postDelayed(
            {
                if (currentUser == null) {
                    sendUserToLogin();
                } else {
                    sendUserToHome();
                }
            }, 3000
        )


        return view;
    }

}