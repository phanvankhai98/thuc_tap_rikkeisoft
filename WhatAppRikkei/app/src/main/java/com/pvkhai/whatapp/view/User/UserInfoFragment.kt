package com.pvkhai.whatapp.view.User

import android.app.Activity.RESULT_OK
import android.app.DatePickerDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.pvkhai.whatapp.R
import com.pvkhai.whatapp.databinding.FragmentUserInfoBinding
import com.pvkhai.whatapp.service.FirebaseService
import com.pvkhai.whatapp.util.LoadingDialog
import com.pvkhai.whatapp.viewmodel.UserInfoViewModel
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.fragment_user_info.*
import kotlinx.android.synthetic.main.fragment_user_info.view.*


class UserInfoFragment : Fragment() {
    var firebase = FirebaseService();
    lateinit var binding: FragmentUserInfoBinding;
    lateinit var loadingDialog: LoadingDialog;
    val userInfoViewModel: UserInfoViewModel by viewModels()
    val GALLARY_SELECT = 1;

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
//        val view = inflater.inflate(R.layout.fragment_user_info, container, false);

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_user_info, container, false);
        binding.lifecycleOwner = viewLifecycleOwner
        binding.userInfo = userInfoViewModel;
        val view = binding.root;
        loadingDialog = activity?.let { LoadingDialog(it) }!!
        retriveUserInfo()

        handleEvent(view);
        view.edt_dateofbirth.keyListener = null;
        view.edt_phonenumber.inputType = InputType.TYPE_CLASS_NUMBER
        return binding.root;
    }

    private fun retriveUserInfo() {
        firebase.rootRef.child("Users").child(firebase.getID())
            .addValueEventListener(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.exists()) {
                        binding.userInfo?.fullName?.value =
                            snapshot.child("name").value.toString();
                        binding.userInfo?.phoneNumber?.value =
                            snapshot.child("numberphone").value.toString();
                        binding.userInfo?.dateOfBirth?.value =
                            snapshot.child("dateofbirth").value.toString();
                        binding.userInfo?.imageUser?.value =
                            snapshot.child("image").value.toString();
                        if (circleImageView !=null) Picasso.get().load(binding.userInfo?.imageUser?.value.toString()).into(circleImageView)
                    } else {
                        binding.userInfo?.fullName?.value = ""
                        binding.userInfo?.phoneNumber?.value = ""
                        binding.userInfo?.dateOfBirth?.value = ""
                        binding.userInfo?.imageUser?.value = ""
                    }
                }
            })
    }

    private fun handleEvent(view: View) {

        view.circleImageView.setOnClickListener { v ->
        }

        view.img_back.setOnClickListener { v ->
            v.findNavController().navigateUp();
        }
        view.img_change_avatar.setOnClickListener {
            updateAvatar(it);
        }
        view.edt_dateofbirth.setOnClickListener {
            updatedate()
        }

        view.tv_save.setOnClickListener { v ->
            firebase.rootRef.child("Users").child(firebase.getID())
                .addValueEventListener(object : ValueEventListener {
                    override fun onCancelled(error: DatabaseError) {
                        TODO("Not yet implemented")
                    }

                    override fun onDataChange(snapshot: DataSnapshot) {
                        if (snapshot.exists()) {
                            binding.userInfo?.imageUser?.value =
                                snapshot.child("image").value.toString();
                            binding.userInfo?.imageCover?.value = snapshot.child("imageCover").value.toString()
                            if (circleImageView != null)
                                Picasso.get().load(binding.userInfo?.imageUser?.value.toString()).into(circleImageView)
                            var profile = HashMap<String, String>();
                            profile.put("uid", firebase.getUser()?.uid!!)
                            profile.put("name", binding.userInfo?.fullName?.value.toString());
                            profile.put("status", "1");
                            profile.put("numberphone",binding.userInfo?.phoneNumber?.value.toString());
                            profile.put("dateofbirth",binding.userInfo?.dateOfBirth?.value.toString());
                            profile.put("image", snapshot.child("image").value.toString());
                            profile.put("imageCover",snapshot.child("imageCover").value.toString());
                            firebase.rootRef.child("Users").child(firebase.getUser()?.uid!!)
                                .setValue(profile)
                        }
                    }
                })
            v.findNavController().navigateUp()
        }
    }

    private fun updatedate() {
        var birthday = edt_dateofbirth.text.toString().split("/")
        val year = birthday[2].toInt()
        val month = birthday[1].toInt()-1
        val day = birthday[0].toInt()

        val datePickerDialog = DatePickerDialog(requireContext(),DatePickerDialog.OnDateSetListener { view, mYear, mMonth, mDay ->
                binding.userInfo?.dateOfBirth?.value = "" + mDay + "/" + (mMonth + 1) + "/" + mYear
        },year,month,day)
        datePickerDialog.show()
    }


    private fun updateAvatar(v: View) {
        var gallaryIntent: Intent = Intent();
        gallaryIntent.action = Intent.ACTION_GET_CONTENT;
        gallaryIntent.type = "image/*";
        startActivityForResult(gallaryIntent, GALLARY_SELECT);
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        loadingDialog.startLoadingDialog();
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GALLARY_SELECT) {
            if (resultCode == RESULT_OK && data != null) {
                var imageUri: Uri = data.data!!;


                var filePath = firebase.userImageProfileRef.child(firebase.getID() + ".jpg")


                filePath.putFile(imageUri).addOnCompleteListener { task ->
                    if (task.isSuccessful) {

                        Toast.makeText(activity,R.string.upload_image_successfull,Toast.LENGTH_LONG).show();

                        filePath.downloadUrl.addOnSuccessListener { uri ->
                            var downloadUrl:Uri = uri
                            firebase.rootRef.child("Users").child(firebase.getID()).child("image")
                                .setValue(downloadUrl.toString())
                                .addOnCompleteListener{ task ->

                                    if (task.isSuccessful) {
                                        Picasso.get().load(downloadUrl.toString()).into(circleImageView)
                                        firebase.rootRef.child("Users").child(firebase.getID())
                                            .addValueEventListener(object : ValueEventListener {
                                                override fun onCancelled(error: DatabaseError) {
                                                    TODO("Not yet implemented")
                                                }

                                                override fun onDataChange(snapshot: DataSnapshot) {
                                                    if (snapshot.exists()) {
                                                        binding.userInfo?.fullName?.value =
                                                            snapshot.child("name").value.toString();
                                                        binding.userInfo?.phoneNumber?.value =
                                                            snapshot.child("numberphone").value.toString();
                                                        binding.userInfo?.dateOfBirth?.value =
                                                            snapshot.child("dateofbirth").value.toString();
                                                        binding.userInfo?.imageUser?.value =
                                                            downloadUrl.toString()
                                                    }
                                                }
                                            })
                                    }

                                    else {
                                    }
                                }
                        }

                    } else
                        Toast.makeText(activity, R.string.upload_image_fail, Toast.LENGTH_SHORT).show();
                    loadingDialog.dismissLoadingDialog();

                }
            }
            else{
                loadingDialog.dismissLoadingDialog();
            }
        }
    }
}
