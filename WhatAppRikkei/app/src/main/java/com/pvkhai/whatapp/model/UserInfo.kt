package com.pvkhai.whatapp.model

import java.io.Serializable

class UserInfo : Serializable{
    lateinit var dateofbirth: String;
    lateinit var image: String;
    lateinit var name: String;
    lateinit var numberphone: String;
    lateinit var status:String;
    lateinit var uid:String;
    var chat: Chat = Chat();
    var request: String = "";
    var unreadMes = 0;
    constructor(){

    }
    constructor(uid:String){
        this.uid = uid;
    }

    constructor(dateofbirth:String,image: String, name: String,numberphone: String,status:String,uid:String){
        this.dateofbirth= dateofbirth;
        this.image = image;
        this.name = name;
        this.numberphone = numberphone;
        this.status = status;
        this.uid = uid;
    }


}