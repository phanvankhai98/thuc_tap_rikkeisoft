package com.pvkhai.whatapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.RecyclerView
import com.pvkhai.whatapp.R
import com.pvkhai.whatapp.model.Chat
import com.pvkhai.whatapp.model.UserInfo
import com.pvkhai.whatapp.service.FirebaseService
import com.pvkhai.whatapp.util.FuntionUtil
import com.pvkhai.whatapp.viewmodel.ChatViewModel
import com.pvkhai.whatapp.viewmodel.FriendViewModel
import com.squareup.picasso.Picasso

class RecyclerChatAdapter(
    val data: ArrayList<Chat>,
    val context: Context,
    val userInfo: UserInfo,
    val chatViewModel: ChatViewModel
) : RecyclerView.Adapter<RecyclerChatAdapter.ChatViewHolder>() {


    val firebase: FirebaseService = FirebaseService();
    val uid = firebase.getID();
    var status = "";

    class ChatViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imgAvatar = view.findViewById<ImageView>(R.id.img_avatar_chat);
        val tvReceive = view.findViewById<TextView>(R.id.tv_receive);
        val tvTimeReceived = view.findViewById<TextView>(R.id.tv_time_receive);
        val tvTimeSent = view.findViewById<TextView>(R.id.tv_time_sent);
        val tvSent = view.findViewById<TextView>(R.id.tv_sent);

        val lnReceived = view.findViewById<ConstraintLayout>(R.id.ln_received);
        val lnSent = view.findViewById<ConstraintLayout>(R.id.ln_sent);
        val imgSent: ImageView = view.findViewById(R.id.img_sent);
        val imgReceived: ImageView = view.findViewById(R.id.img_received);
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatViewHolder {
        val contactView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_chat, parent, false);
        return ChatViewHolder(contactView);
    }

    override fun getItemCount(): Int {
        return data.size;
    }

    override fun onBindViewHolder(holder: ChatViewHolder, position: Int) {
        var mChat = data[position];
        var nextChat = Chat();
        var preChat = Chat();

        if (position > 0) {
            preChat = data[position - 1];
        }
        if (data.size > 1 && position < data.size - 1) {
            nextChat = data[position + 1]
        }
        Picasso.get().load(userInfo.image).into(holder.imgAvatar);

        //set image or text
        if (mChat.type == "text") {
            holder.tvSent.text = mChat.message
            holder.tvReceive.text = mChat.message
            holder.tvSent.visibility = View.VISIBLE
            holder.tvReceive.visibility = View.VISIBLE
            holder.imgSent.visibility = View.GONE
            holder.imgReceived.visibility = View.GONE

            if (firebase.getID() == mChat.form) {
                holder.lnSent.visibility = View.VISIBLE;
                holder.lnReceived.visibility = View.INVISIBLE;
                if (firebase.getID() != preChat.form && firebase.getID() == nextChat.form) {
                    holder.tvSent.setBackgroundResource(R.drawable.custom_bg_sendbox2_end);
                } else if (firebase.getID() == preChat.form && firebase.getID() == nextChat.form) {
                    holder.tvSent.setBackgroundResource(R.drawable.custom_bg_sendbox2next);
                } else if (firebase.getID() == preChat.form && firebase.getID() != nextChat.form) {
                    holder.tvSent.setBackgroundResource(R.drawable.custom_bg_sendbox2_start);
                } else {
                    holder.tvSent.setBackgroundResource(R.drawable.custom_bg_sendbox2);
                }
            } else {
                holder.lnSent.visibility = View.INVISIBLE;
                holder.lnReceived.visibility = View.VISIBLE;
                if (firebase.getID() == preChat.form && firebase.getID() != nextChat.form) {
                    holder.tvReceive.setBackgroundResource(R.drawable.custom_bg_sendbox_end);
                } else if (firebase.getID() != preChat.form && firebase.getID() != nextChat.form) {
                    holder.tvReceive.setBackgroundResource(R.drawable.custom_bg_sendboxnext);
                    holder.imgAvatar.visibility = View.INVISIBLE;
                } else if (firebase.getID() != preChat.form && firebase.getID() == nextChat.form) {
                    holder.tvReceive.setBackgroundResource(R.drawable.custom_bg_sendbox_start);
                    holder.imgAvatar.visibility = View.INVISIBLE;
                } else {
                    holder.tvReceive.setBackgroundResource(R.drawable.custom_bg_sendbox);
                }
            }
        } else if (mChat.type == "image") {
            holder.tvSent.visibility = View.GONE
            holder.tvReceive.visibility = View.GONE
            holder.imgSent.visibility = View.VISIBLE
            holder.imgReceived.visibility = View.VISIBLE

            if (firebase.getID() == mChat.form) {
                Picasso.get().load(mChat.message)
                    .placeholder(R.drawable.progress_animation)
                    .into(holder.imgSent)
                holder.lnSent.visibility = View.VISIBLE;
                holder.lnReceived.visibility = View.INVISIBLE;
            } else {
                Picasso.get().load(mChat.message)
                    .placeholder(R.drawable.progress_animation)
                    .into(holder.imgReceived)
                holder.lnSent.visibility = View.INVISIBLE;
                holder.lnReceived.visibility = View.VISIBLE;
            }
        } else if (mChat.type == "sticker") {
            holder.tvSent.visibility = View.GONE
            holder.tvReceive.visibility = View.GONE
            holder.imgSent.visibility = View.VISIBLE
            holder.imgReceived.visibility = View.VISIBLE


            if (firebase.getID() == mChat.form) {
                holder.imgSent.setImageResource(
                    context.resources.getIdentifier(
                        mChat.message,
                        "mipmap",
                        context.packageName
                    )
                );
                holder.lnSent.visibility = View.VISIBLE;
                holder.lnReceived.visibility = View.INVISIBLE;
            } else {
                holder.imgReceived.setImageResource(
                    context.resources.getIdentifier(
                        mChat.message,
                        "mipmap",
                        context.packageName
                    )
                );
                holder.lnSent.visibility = View.INVISIBLE;
                holder.lnReceived.visibility = View.VISIBLE;
            }
        }

        holder.imgSent.setOnClickListener {
            if (mChat.type == "image")
                chatViewModel.image.value = mChat.message;
        }
        holder.imgReceived.setOnClickListener {
            if (mChat.type == "image")
                chatViewModel.image.value = mChat.message;
        }

        holder.tvReceive.setOnClickListener {
            holder.tvTimeReceived.text = FuntionUtil.getTime(mChat.time);
            if (holder.tvTimeReceived.isVisible)
                holder.tvTimeReceived.visibility = View.GONE;
            else
                holder.tvTimeReceived.visibility = View.VISIBLE;
        }

        holder.tvSent.setOnClickListener {
            holder.tvTimeSent.text = FuntionUtil.getTime(mChat.time);
            if (holder.tvTimeSent.isVisible)
                holder.tvTimeSent.visibility = View.GONE;
            else
                holder.tvTimeSent.visibility = View.VISIBLE;

        }

        //set chat

    }
}
