package com.pvkhai.whatapp.view.Home

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.plusAssign
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.NavigationUI.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.pvkhai.whatapp.R
import com.pvkhai.whatapp.view.Friend.FriendFragment
import com.pvkhai.whatapp.view.Message.MessageFragment
import com.pvkhai.whatapp.view.User.UserFragment
import kotlinx.android.synthetic.main.fragment_home.view.*


class HomeFragment : Fragment() {


    private val fragmentMessage = MessageFragment()
    private val fragmentUser = UserFragment()
    private val fragmentFriend = FriendFragment()
    var active: Fragment = fragmentMessage;
    lateinit var fm: FragmentManager;
    lateinit var navController: NavController

    private var mContainerView: View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init();
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (mContainerView == null) {
            mContainerView = inflater.inflate(R.layout.fragment_home, container, false);
        } else {
            val parent = mContainerView?.parent
            if (parent != null) {
                (parent as? ViewGroup)?.removeView(mContainerView)
            }
        }
//        val view =
//        var mapFrag:NavHostFragment = childFragmentManager.findFragmentById(
//        R.id.fragment_nav_host) as NavHostFragment
//        NavigationUI.setupWithNavController(view.navigation_view , mapFrag.navController)


        fm.beginTransaction().hide(active).show(active).commit();

        val navigationView: BottomNavigationView = mContainerView?.findViewById(R.id.navigation_view)!!;
        val selectNavigation = BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.message -> {
                    fm.beginTransaction().hide(active).show(fragmentMessage).commit()
                    active = fragmentMessage;
                    true
                }
                R.id.user -> {
                    fm.beginTransaction().hide(active).show(fragmentUser).commit()
                    active = fragmentUser;
                    true
                }
                R.id.friend -> {
                    fm.beginTransaction().hide(active).show(fragmentFriend).commit()
                    active = fragmentFriend;
                    true
                }
                else -> false
            }
        }
        navigationView.setOnNavigationItemSelectedListener(selectNavigation);


        return mContainerView
    }

//    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        menuInflater.inflate(R.menu.menu_bottom,menu)
//        bottomBar.setupWithNavController(menu!!,navController)
//        return true
//    }

//    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
//        super.onCreateOptionsMenu(menu, inflater)
//
//    }


    private fun init() {

        fm = activity?.supportFragmentManager!!
        if (!(fragmentFriend.isAdded || fragmentUser.isAdded || fragmentMessage.isAdded)) {
            fm.beginTransaction().add(R.id.frame_container, fragmentUser, "3").hide(fragmentUser)
                .commit();
            fm.beginTransaction().add(R.id.frame_container, fragmentFriend, "2").hide(fragmentFriend)
                .commit();
            fm.beginTransaction().add(R.id.frame_container, fragmentMessage, "1").hide(fragmentMessage)
                .commit();
        }

    }


}