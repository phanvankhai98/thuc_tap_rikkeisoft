package com.pvkhai.whatapp.adapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.pvkhai.whatapp.view.Friend.AllFragment
import com.pvkhai.whatapp.view.Friend.AllFriendFragment
import com.pvkhai.whatapp.view.Friend.RequestFragment

class FriendViewpagerAdapter(fragment: Fragment): FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int {
        return 3;
    }

    override fun createFragment(position: Int): Fragment {
        when (position) {
            0 -> return AllFriendFragment()
            1 -> return AllFragment()
            2 -> return RequestFragment()
        }
        return AllFragment()
    }

}
//    val eventList = listOf("0", "1", "2")
//
//    // Layout "layout_demo_viewpager2_cell.xml" will be defined later
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
//        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.layout_demo_viewpager2_cell, parent, false))
//
//    override fun getItemCount() = eventList.count()
//    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
//        (holder.view as? TextView)?.also{
//            it.text = "Page " + eventList.get(position)
//
//            val backgroundColorResId = if (position % 2 == 0) R.color.blue else R.color.orange)
//            it.setBackgroundColor(ContextCompat.getColor(it.context, backgroundColorResId))
//        }
//    }
//
//    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)