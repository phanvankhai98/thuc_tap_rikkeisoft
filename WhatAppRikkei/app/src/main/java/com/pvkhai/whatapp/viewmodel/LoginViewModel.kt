package com.pvkhai.whatapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pvkhai.whatapp.model.User

class LoginViewModel() : ViewModel() {
    var userName: MutableLiveData<String> = MutableLiveData();
    var password: MutableLiveData<String> = MutableLiveData();

    var userMutableLiveData: MutableLiveData<User> = MutableLiveData();

    fun geUser(): MutableLiveData<User> {
        return userMutableLiveData;
    }

    fun onClick(){
        var user =
            User(userName.value!!, password.value!!);
        userMutableLiveData.value = user;
    }

//    public void onClick(View view) {
//
//        LoginUser loginUser = new LoginUser(EmailAddress.getValue(), Password.getValue());
//
//        userMutableLiveData.setValue(loginUser);
//
//    }
}