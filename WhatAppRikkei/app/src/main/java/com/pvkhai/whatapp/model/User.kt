package com.pvkhai.whatapp.model

import android.util.Patterns

class User(var username: String,var password: String){
    var fullname:String = ""

    constructor(username: String, password: String, fullname:String) : this(username,password) {
        this.fullname = fullname;
    }


    fun isEmailValid (): Boolean {
//        return Patterns.EMAIL_ADDRESS.matcher(username).matches();
        return Patterns.EMAIL_ADDRESS.matcher(username).matches();
    }
    fun isPasswordLengthGreaterThan5 (): Boolean{
        return password.length> 5;
    }
}