package com.pvkhai.whatapp.view.Friend

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.google.android.material.tabs.TabLayoutMediator
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.pvkhai.whatapp.R
import com.pvkhai.whatapp.adapter.FriendViewpagerAdapter
import com.pvkhai.whatapp.databinding.FragmentFriendBinding
import com.pvkhai.whatapp.model.UserInfo
import com.pvkhai.whatapp.service.FirebaseService
import com.pvkhai.whatapp.viewmodel.FriendViewModel
import kotlinx.android.synthetic.main.fragment_friend.view.*


class FriendFragment : Fragment() {
    val NOT_EXIST = -1;
    lateinit var binding: FragmentFriendBinding;
    var firebase = FirebaseService();
    var data = ArrayList<UserInfo>();
    var countRequest = 0;
    private val friendViewModel: FriendViewModel by activityViewModels();


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_friend, container, false);
        val view = binding.root;
        binding.lifecycleOwner = viewLifecycleOwner
        binding.search = friendViewModel;
        getAll();

        handleTabLayout(view);


        return binding.root;
    }

    
    private fun getAll() {
        firebase.userRef.orderByChild("name")
            .addValueEventListener(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    data.clear()
                    countRequest = 0;
                    if (snapshot.exists())
                        for (child: DataSnapshot in snapshot.children) {
                            var userInfo = child.getValue(UserInfo::class.java);
                            findFriendStatus(userInfo!!);
                        }
                    if (data.size == 0) {
                        friendViewModel.listUser.value = data;
                    }
                }
            })
    }

    private fun findFriendStatus(userInfo: UserInfo) {
//        data.add(userInfo);
        firebase.contactsRef.child(firebase.getID()).child(userInfo.uid)
            .addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    val positon = data.indexOf(userInfo);
                    if (snapshot.exists()) {
                        userInfo.request = snapshot.child("Contacts").value.toString();
                        if (positon != NOT_EXIST)
                            data[positon] = userInfo;
                        else
                            data.add(userInfo);
                    } else {
                        if (positon != NOT_EXIST) {
                            userInfo.request = "";
                            data[positon] = userInfo;
                        } else
                            data.add(userInfo)
                    }
                    friendViewModel.listUser.value = data;
                }

                override fun onCancelled(error: DatabaseError) {
                }
            })
    }

    private fun handleTabLayout(view: View) {
        val tmp = requireContext().resources.getStringArray(R.array.title);
        view.pager.adapter = FriendViewpagerAdapter(this)

        TabLayoutMediator(view.tablayout, view.pager) { tab, position ->

            tab.setCustomView(R.layout.notification_badge);
            tab.customView?.findViewById<TextView>(R.id.tv_titletab)?.text = tmp[position].toUpperCase()

            if (position == 2) {
                var badge = tab.customView?.findViewById<TextView>(R.id.text)
                getCountRequest(badge);

            }
            view.pager.setCurrentItem(tab.position, true)
        }.attach()

    }

    private fun getCountRequest(badge: TextView?) {
        friendViewModel.listUser.observe(viewLifecycleOwner, Observer {
            countRequest = 0;
            it.forEach {
                if (it.request == "Received")
                    countRequest++;
            }
            if (countRequest != 0) {
                badge?.isVisible = true
                badge?.text = countRequest.toString();
            } else {
                badge?.isVisible = false
            }
        })
    }

}