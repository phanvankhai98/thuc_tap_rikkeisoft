package com.pvkhai.whatapp.util

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.content.res.Configuration
import android.content.res.Resources
import android.util.DisplayMetrics
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.widget.Toast
import androidx.navigation.findNavController
import com.pvkhai.whatapp.R
import kotlinx.android.synthetic.main.custom_dialog_language.view.*
import java.util.*

class LanguageDialog (val activity: Activity) {
    var dialog: AlertDialog? = null;
    var locale: Locale? = null;
    fun startLanguageDialog(){
        var arrL = arrayOf<String>("Tiếng Việt","English")
        var builder: AlertDialog.Builder = AlertDialog.Builder(activity);
        val inflater: LayoutInflater = activity.layoutInflater;

        builder.setView(inflater.inflate(R.layout.custom_dialog_language,null));
        builder.setCancelable(true);
        builder.setTitle(R.string.selectLanguge)

        builder.setItems(arrL,DialogInterface.OnClickListener { dialogInterface, i ->
            if (i==0){
                setLocale("vi")
            }
            if (i==1){
                setLocale("en")
            }
        })

        dialog = builder.create();
        dialog?.show();
    }

    private fun setLocale(lang: String) {
        val locale = Locale(lang)
        Locale.setDefault(locale)
        val config = Configuration()
        config.locale = locale
        activity.resources.updateConfiguration(config,activity.resources.displayMetrics)

        val sharedPreferences: SharedPreferences = activity.getSharedPreferences("Settings",Activity.MODE_PRIVATE)
        val editor: SharedPreferences.Editor = sharedPreferences.edit();

        editor.putString("App Language",lang)
        editor.apply()

        activity.recreate()

    }



    fun dismissLanguageDialog(){
        dialog?.dismiss()
    }
}