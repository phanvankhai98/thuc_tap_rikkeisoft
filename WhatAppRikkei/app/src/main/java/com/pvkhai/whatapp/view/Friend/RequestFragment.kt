package com.pvkhai.whatapp.view.Friend

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.pvkhai.whatapp.R
import com.pvkhai.whatapp.adapter.RecyclerRequestAdapter
import com.pvkhai.whatapp.model.UserInfo
import com.pvkhai.whatapp.service.FirebaseService
import com.pvkhai.whatapp.viewmodel.FriendViewModel
import kotlinx.android.synthetic.main.fragment_all_friend.view.*

class RequestFragment : Fragment() {
    val firebase = FirebaseService();
    lateinit var adapter: RecyclerRequestAdapter
    var data = ArrayList<UserInfo>();
    private var listSent = ArrayList<UserInfo>()
    private var listReceived = ArrayList<UserInfo>()

    private val friendViewModel: FriendViewModel by activityViewModels();
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_all_friend, container, false);

        friendViewModel.listUser.observe(viewLifecycleOwner, Observer {listUser->
            data.clear();
            listSent.clear();
            listReceived.clear();
            listUser.forEach {
                if (it.request == "Received") listReceived.add(it);
                if (it.request == "Sent") listSent.add(it);
            }

            data.addAll(listReceived);
            data.addAll(listSent);
            adapter = RecyclerRequestAdapter(data, requireContext());

            val layoutManager = LinearLayoutManager(context);
            view.rc_friend.layoutManager = layoutManager;
            view.rc_friend.adapter = adapter;

            view.empty.isVisible = data.size == 0;

            friendViewModel.search.observe(viewLifecycleOwner, Observer { key ->
                data.clear();
                listSent.clear();
                listReceived.clear();
                listUser.forEach { user ->
                    if (user.name.toLowerCase().contains(key.trim().toLowerCase())){
                        if (user.request == "Received") listReceived.add(user);
                        if (user.request == "Sent") listSent.add(user);
                    }
                }
                data.addAll(listReceived);
                data.addAll(listSent);
                adapter.notifyDataSetChanged();
                view.empty.isVisible = data.size == 0;
            })
        })


        return view;
    }

    private fun getAll(searchKey: String) {
        firebase.userRef.orderByChild("name").startAt(searchKey).endAt("$searchKey\uf8ff")
            .addValueEventListener(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    data.clear()
                    if (snapshot.exists())
                        for (child: DataSnapshot in snapshot.children) {
                            var userInfo = child.getValue(UserInfo::class.java);
                            findRequestFriend(userInfo!!);
                        }
                    if (data.size == 0) {
                        adapter.notifyDataSetChanged();
                    }
                }
            })
    }

    private fun findRequestFriend(userInfo: UserInfo) {
        firebase.chatRequestRef.child(firebase.getID())
            .addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.exists()) {
                        snapshot.children.forEach {
                            if (it.key == userInfo.uid) {
                                userInfo.request = snapshot.child("request_type").value.toString();
                                data.add(userInfo)
                                adapter.notifyDataSetChanged();
                            }
                        }

                    } else {
                        data.clear();
                        adapter.notifyDataSetChanged();
                    }
                }

                override fun onCancelled(error: DatabaseError) {

                }
            })
    }
//    fun getDxata() {
//        firebase.chatRequestRef.child(firebase.getID()).addValueEventListener(object : ValueEventListener {
//            override fun onCancelled(error: DatabaseError) {
//
//            }
//
//            override fun onDataChange(snapshot: DataSnapshot) {
//                data.clear()
//                snapshot.children.forEach {
////                    Toast.makeText(context, it.key.toString(), Toast.LENGTH_LONG).show();
//                    var request = ChatRequest(it.key.toString(),it.child("request_type").value.toString());
//                    data.add(request);
//                }
//                data.sortBy { it.request.toString() }
//                adapter.notifyDataSetChanged();
//            }
//
//        })
//    }

    private fun setupView(view: View?) {
        firebase.chatRequestRef.child(firebase.getID()).addValueEventListener(object :
            ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                view?.empty?.isVisible = !snapshot.exists()
            }
        })
    }
}