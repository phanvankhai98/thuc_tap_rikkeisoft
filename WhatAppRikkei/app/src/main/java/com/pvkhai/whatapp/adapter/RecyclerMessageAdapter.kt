package com.pvkhai.whatapp.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.pvkhai.whatapp.R
import com.pvkhai.whatapp.model.UserInfo
import com.pvkhai.whatapp.service.FirebaseService
import com.pvkhai.whatapp.util.FuntionUtil
import com.squareup.picasso.Picasso
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class RecyclerMessageAdapter(val data: ArrayList<UserInfo>, val context: Context) :
    RecyclerView.Adapter<RecyclerMessageAdapter.MyViewHolder>() {
    val firebase: FirebaseService = FirebaseService();
    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tvName = view.findViewById<TextView>(R.id.tv_name);
        var tvMessage = view.findViewById<TextView>(R.id.tv_message);
        var imgAvatar = view.findViewById<ImageView>(R.id.img_avatar);
        var tvTime = view.findViewById<TextView>(R.id.tv_time);
        var tvBadge = view.findViewById<TextView>(R.id.tv_badge_mess);
        var lnMessage = view.findViewById<LinearLayout>(R.id.lnMessage);
        var fmBadge = view.findViewById<FrameLayout>(R.id.badge_mess);

    }

    override fun getItemCount(): Int {
        return data.size;
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val context = parent.context;
        val contactView =
            LayoutInflater.from(context).inflate(R.layout.item_message, parent, false);
        return MyViewHolder(contactView);
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mUser = data[position];
        val tsLong = System.currentTimeMillis()
        var time = ""
        var message :String;
        if(mUser.chat.type == "text") message = mUser.chat.message
        else if(mUser.chat.type == "image") message =  context.getString(R.string.sent_a_photo)
        else message =  context.getString(R.string.sent_a_sticker)

        val differenceTime = tsLong - mUser.chat.time.toLong()
        holder.tvName.text = mUser.name;
        Picasso.get().load(mUser.image).placeholder(R.mipmap.ic_placeholder)
            .into(holder.imgAvatar);

        //set message

        if(mUser.chat.form ==firebase.getID() ){
            holder.tvMessage.text = "Bạn: $message"
        }
        else{
            holder.tvMessage.text = message;
        }

        //set badge
        if (mUser.unreadMes == 0) {
            holder.fmBadge.visibility = View.GONE;
            holder.tvMessage.setTextColor(context.resources.getColor(R.color.colorGrayDark))
            holder.tvTime.setTextColor(context.resources.getColor(R.color.colorGrayDark))
        } else {
            holder.tvBadge.text = mUser.unreadMes.toString();
            holder.fmBadge.visibility = View.VISIBLE
            holder.tvMessage.setTextColor(context.resources.getColor(R.color.colorBlack))
            holder.tvTime.setTextColor(context.resources.getColor(R.color.colorBlack))
        }

        //set thoi gian
        if (differenceTime <= 86400000)
            time = FuntionUtil.convertTimestampToTime(mUser.chat.time)
        else if (differenceTime < 86400000 * 2)
            time = "Hôm qua"
        else
            time = FuntionUtil.convertTimestampToDate(mUser.chat.time);

        holder.tvTime.text = time
        holder.lnMessage.setOnClickListener {
            var bundle = bundleOf("userinfo" to data[position])
            it.findNavController().navigate(R.id.action_homeFragment_to_chatFragment, bundle)
        }


    }


}