package com.pvkhai.whatapp.view.Message

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.pvkhai.whatapp.R
import com.pvkhai.whatapp.adapter.RecyclerChatAdapter
import com.pvkhai.whatapp.adapter.StagAdapter
import com.pvkhai.whatapp.databinding.FragmentChatBinding
import com.pvkhai.whatapp.model.Chat
import com.pvkhai.whatapp.model.UserInfo
import com.pvkhai.whatapp.service.FirebaseService
import com.pvkhai.whatapp.viewmodel.ChatViewModel
import com.pvkhai.whatapp.viewmodel.FriendViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_chat.view.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class ChatFragment : Fragment() {

    lateinit var userInfo: UserInfo;
    val firebase = FirebaseService()
    var data = ArrayList<Chat>();
    lateinit var adapter: RecyclerChatAdapter;
    private val GALLARY_SELECT = 1;

    private val chatViewModel: ChatViewModel by activityViewModels();
    lateinit var binding: FragmentChatBinding;
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
//        val view = inflater.inflate(R.layout.fragment_chat, container, false);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat, container, false);
        val view = binding.root;
        init(view);

        var data = requireContext().resources.getStringArray((R.array.sticker));
        val adapterSticker = StagAdapter(data, requireContext(), userInfo);
        view.rc_sticker.layoutManager = GridLayoutManager(requireContext(), 4);
        binding.rcSticker.adapter = adapterSticker;


        getAllChat(view);
        handleEvent(view);
        return view;
    }

    private fun checkSeen(view: View) {
        firebase.messagesRef.child(firebase.getID()).child(userInfo.uid)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    snapshot.children.forEach {
                        var mChat: Chat = it.getValue(Chat::class.java)!!;
                        if (!mChat.checkseen) {
                            mChat.checkseen = true;
                            firebase.messagesRef.child(firebase.getID())
                                .child(userInfo.uid)
                                .child(it.key.toString())
                                .updateChildren(mChat.toMap()!!);
                        }
                    }

                }

            })
    }

    private fun getAllChat(view: View) {

        firebase.messagesRef.child(firebase.getID()).child(userInfo.uid)
            .addValueEventListener(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    data.clear();
                    if (snapshot.exists()) {
                        snapshot.children.forEach {
                            var mChat: Chat = it.getValue(Chat::class.java)!!;
                            data.add(mChat);
                        }
                    }
                    chatViewModel.listChat.value = data;

                }

            })
    }

    private fun handleEvent(view: View) {
        view.img_back.setOnClickListener {
            it.findNavController().navigateUp();
        }
        view.img_sent.setOnClickListener {
            sendMessage(view);
        }
        view.img_pick_photo.setOnClickListener {
            sendImage(view);
        }
        view.edt_message.setOnClickListener {
            view.ln_sticker.visibility = View.GONE;
        }
        view.img_sticker.setOnClickListener {
            closeKeyboard()
            view.ln_sticker.isVisible = !view.ln_sticker.isVisible
            if (view.ln_sticker.isVisible) {
                view.img_sticker.setImageDrawable(requireContext().getDrawable(R.mipmap.ic_emoji2))
            } else {
                view.img_sticker.setImageDrawable(requireContext().getDrawable(R.mipmap.ic_emoji))
            }
        }
    }

    private fun closeKeyboard() {
        var v = activity?.currentFocus;
        var im = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager;
        im.hideSoftInputFromWindow(v?.windowToken, 0)
    }

    private fun sendImage(view: View) {
        var gallaryIntent: Intent = Intent();
        gallaryIntent.action = Intent.ACTION_GET_CONTENT;
        gallaryIntent.type = "image/*";
        startActivityForResult(gallaryIntent, GALLARY_SELECT);
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GALLARY_SELECT) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                var imageUri: Uri = data.data!!;

                var messageSendRef = "Messages/" + firebase.getID() + "/" + userInfo.uid;
                var messageReceivedRef = "Messages/" + userInfo.uid + "/" + firebase.getID();

                var userMessageKeyRef =
                    firebase.messagesRef.child(firebase.getID()).child(userInfo.uid).push();

                var messageID = userMessageKeyRef.key;
                var filePath = firebase.imageFileRef.child(messageID + ".jpg")

                filePath.putFile(imageUri).addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        filePath.downloadUrl.addOnSuccessListener { uri ->
                            var downloadUrl: Uri = uri
                            val tsLong = System.currentTimeMillis()

                            var messageBody = HashMap<String, Any>();
                            messageBody["message"] = downloadUrl.toString();
                            messageBody["type"] = "image";
                            messageBody["form"] = firebase.getID();
                            messageBody["time"] = tsLong.toString();
                            messageBody["isshow"] = true;
                            messageBody["checkseen"] = false;

                            var messageDetail = HashMap<String, Any>();
                            messageDetail["$messageSendRef/$messageID"] = messageBody;
                            messageDetail["$messageReceivedRef/$messageID"] = messageBody;

                            firebase.rootRef.updateChildren(messageDetail).addOnCompleteListener {
                                if (it.isSuccessful) {
//                                    view.edt_message.setText("");
                                }
                            }
                        }
                    }
                }


            }
        }
    }

    private fun sendMessage(view: View) {
        var mess = view.edt_message.text.toString();
        val tsLong = System.currentTimeMillis()

        if (TextUtils.isEmpty(mess)) {

        } else {
            var messageSendRef = "Messages/" + firebase.getID() + "/" + userInfo.uid;
            var messageReceivedRef = "Messages/" + userInfo.uid + "/" + firebase.getID();

            var userMessageKeyRef =
                firebase.messagesRef.child(firebase.getID()).child(userInfo.uid).push();

            var messageID = userMessageKeyRef.key;

            var messageBody = HashMap<String, Any>();
            messageBody["message"] = mess;
            messageBody["type"] = "text";
            messageBody["form"] = firebase.getID();
            messageBody["time"] = tsLong.toString();
            messageBody["isshow"] = true;
            messageBody["checkseen"] = false;

            var messageDetail = HashMap<String, Any>();
            messageDetail["$messageSendRef/$messageID"] = messageBody;
            messageDetail["$messageReceivedRef/$messageID"] = messageBody;

            firebase.rootRef.updateChildren(messageDetail).addOnCompleteListener {
                if (it.isSuccessful) {
                    view.edt_message.setText("");
                }
            }
        }
    }

    private fun init(view: View) {
        userInfo = arguments?.getSerializable("userinfo") as UserInfo
        view.tv_name.text = userInfo.name;
        Picasso.get().load(userInfo.image).into(view.img_avatar);

        chatViewModel.listChat.observe(viewLifecycleOwner, Observer {
            checkSeen(view);
            it.reverse()
            adapter = RecyclerChatAdapter(it, requireContext(), userInfo, chatViewModel);

            val layoutManager = LinearLayoutManager(context);
            layoutManager.reverseLayout = true
            layoutManager.stackFromEnd = false;
            view.rc_message.layoutManager = layoutManager;
            view.rc_message.adapter = adapter;

//            view.rc_message.smoothScrollToPosition(0)
//
        })

        chatViewModel.image.observe(viewLifecycleOwner, Observer {
            if (it == "")
                view.img_viewimage.visibility = View.GONE;
            else {
                view.img_viewimage.visibility = View.VISIBLE;
                Picasso.get().load(it).into(view.photo_view)
            }
        })

        view.img_cancel.setOnClickListener {
            view.img_viewimage.visibility = View.GONE;
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()

        chatViewModel.image.value = "";
    }
}