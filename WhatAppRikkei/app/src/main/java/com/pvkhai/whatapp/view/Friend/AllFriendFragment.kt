package com.pvkhai.whatapp.view.Friend

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.pvkhai.whatapp.R
import com.pvkhai.whatapp.adapter.RecyclerAllAdapter
import com.pvkhai.whatapp.model.UserInfo
import com.pvkhai.whatapp.service.FirebaseService
import com.pvkhai.whatapp.viewmodel.FriendViewModel
import kotlinx.android.synthetic.main.fragment_all_friend.view.*

class AllFriendFragment : Fragment() {
    val firebase = FirebaseService();
    var data = ArrayList<UserInfo>();

    val friendViewModel: FriendViewModel by activityViewModels();
    lateinit var adapter: RecyclerAllAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_all_friend, container, false);
//        setupView(view);

//        friendViewModel.search.value = "";

//        friendViewModel.search.observe(viewLifecycleOwner, Observer {
//            getAll(it);
//        })
        friendViewModel.listUser.observe(viewLifecycleOwner, Observer { listUser ->
            data.clear();
            listUser.forEach {
                if (it.request == "Saved") {
                    data.add(it)
                }
            }
            adapter = RecyclerAllAdapter(data, requireContext());

            val layoutManager = LinearLayoutManager(context);
            view.rc_friend.layoutManager = layoutManager;
            view.rc_friend.adapter = adapter;

            view.empty.isVisible = data.size == 0;

            friendViewModel.search.observe(viewLifecycleOwner, Observer { key ->
                data.clear();
                listUser.forEach { user ->
                    if (user.name.toLowerCase().contains(key.trim().toLowerCase())
                        && user.request == "Saved"
                    )
                        data.add(user);
                }
                adapter.notifyDataSetChanged();
                view.empty.isVisible = data.size == 0;
            })
        })



        return view;
    }

}