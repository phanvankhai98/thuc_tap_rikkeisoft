package com.pvkhai.whatapp.view.Login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.pvkhai.whatapp.R
import com.pvkhai.whatapp.util.LoadingDialog
import com.pvkhai.whatapp.databinding.FragmentLoginBinding
import com.pvkhai.whatapp.model.User
import com.pvkhai.whatapp.viewmodel.LoginViewModel
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_login.view.*


class LoginFragment : Fragment() {
    private var currentUser: FirebaseUser? = null;
    lateinit var mAuth: FirebaseAuth;
    lateinit var loading: LoadingDialog;

    lateinit var binding: FragmentLoginBinding;
    private val loginViewModel: LoginViewModel by activityViewModels();

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);

        val view = binding.root;
        init(view);
        binding.lifecycleOwner = viewLifecycleOwner
        binding.user = loginViewModel;

        loginViewModel.geUser().observe(viewLifecycleOwner, Observer { user ->
            if (user?.username.equals("")) {
                binding.edtUsername.error = context?.getString(R.string.fill_email)
                binding.edtUsername.requestFocus();
            } else if (user?.password.equals("")) {
                binding.edtPassword.error = context?.getString(R.string.fill_password)
                binding.edtPassword.requestFocus();
            } else if (!user.isEmailValid()) {
                binding.edtUsername.error = context?.getString(R.string.invalid_username)
                binding.edtUsername.requestFocus();
            } else if (!user.isPasswordLengthGreaterThan5()) {
                binding.edtPassword.error = context?.getString(R.string.invalid_password)
                binding.edtPassword.requestFocus();
            } else {
                allowUserLogin(user)
            }
        })

        handleEvent(view);

        return binding.root;
    }

    private fun init(view: View) {
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth!!.currentUser
        loading = activity?.let { LoadingDialog(it) }!!;
        view.btn_login.isEnabled = false;
    }

    fun buttonActive(): Boolean {
        return !(edt_password.text.toString().equals("") || edt_username.text.toString()
            .equals(""))
    }

    private fun handleEvent(view: View) {
        view.tv_register.setOnClickListener {
            view.findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }
        view.btn_login.setOnClickListener {
            loginViewModel.onClick();
        }
        view.edt_username.addTextChangedListener {
            view.btn_login.isEnabled = buttonActive()
        }
        view.edt_password.addTextChangedListener {
            view.btn_login.isEnabled = buttonActive()

        }

    }


    private fun allowUserLogin(user: User) {

        loading.startLoadingDialog()
        mAuth.fetchSignInMethodsForEmail(user.username).addOnCompleteListener { task ->
            if (task.getResult()?.signInMethods?.size != 0){
                mAuth.signInWithEmailAndPassword(user.username, user.password)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {

                            user.username =""
                            user.password =""
                            edt_username.text = null
                            edt_password.text = null

                            Toast.makeText(context, R.string.login_successfull, Toast.LENGTH_LONG).show();
                            sendUserToHome();
                            loading.dismissLoadingDialog();
                        } else
                            Toast.makeText(context, R.string.infor_failed, Toast.LENGTH_LONG).show();
                        loading.dismissLoadingDialog();
                    }?.addOnCanceledListener {
                        Toast.makeText(context, R.string.login_failed, Toast.LENGTH_LONG).show();
                        loading.dismissLoadingDialog()
                    }
            }
            else {
                Toast.makeText(context, R.string.infor_notexisted, Toast.LENGTH_LONG).show();
                loading.dismissLoadingDialog()
            }
        }

    }

    private fun sendUserToHome() {
        this.findNavController().navigate(
            R.id.action_loginFragment_to_homeFragment, null,
            NavOptions.Builder()
                .setPopUpTo(
                    R.id.loginFragment,
                    true
                ).build()
        )
    }

}