package com.pvkhai.whatapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pvkhai.whatapp.R

class RecyclerFriendAdapter(val data: ArrayList<String>): RecyclerView.Adapter<RecyclerFriendAdapter.MyViewHolder>() {
    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val tvName = view.findViewById<TextView>(R.id.tv_name);
        val tvMessage = view.findViewById<TextView>(R.id.tv_message);
    }

    override fun getItemCount(): Int {
        return data.size;
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val context = parent.context;
        val contactView = LayoutInflater.from(context).inflate(R.layout.item_friend,parent,false);
        return MyViewHolder(contactView);
    }
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

    }


}