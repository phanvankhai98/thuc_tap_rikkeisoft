package com.pvkhai.whatapp.view.Message

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.pvkhai.whatapp.R
import com.pvkhai.whatapp.adapter.RecyclerMessageAdapter
import com.pvkhai.whatapp.databinding.FragmentMessageBinding
import com.pvkhai.whatapp.model.Chat
import com.pvkhai.whatapp.model.UserInfo
import com.pvkhai.whatapp.service.FirebaseService
import com.pvkhai.whatapp.viewmodel.MessageViewModel
import kotlinx.android.synthetic.main.fragment_all_friend.view.*
import kotlinx.android.synthetic.main.fragment_message.view.*
import kotlinx.android.synthetic.main.fragment_message.view.empty
import java.util.*
import kotlin.collections.ArrayList

class MessageFragment : Fragment() {

    val firebase: FirebaseService = FirebaseService();
    lateinit var adapter: RecyclerMessageAdapter;
    var dataUser = ArrayList<UserInfo>();
    var listUser = ArrayList<UserInfo>();

    private val messageViewModel: MessageViewModel by activityViewModels();
    lateinit var binding: FragmentMessageBinding;

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
//        val view = inflater.inflate(R.layout.fragment_message, container, false);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_message, container, false);
        val view = binding.root;
        binding.message = messageViewModel;

        init(view);
        handleEvent(view);
        getAllMess(view);
        return view;
    }

    private fun getAllMess(view: View) {

        firebase.rootRef.child("Messages").child(firebase.getID()).addValueEventListener(
            object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    Toast.makeText(context,"lỗi",Toast.LENGTH_SHORT).show();
                }
                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.exists()) {
                        dataUser.clear();

                        snapshot.children.forEach {
                            var mUser = UserInfo(it.key.toString());
                            mUser.chat = it.children.last().getValue(Chat::class.java)!!;
                            getUserInfo(mUser);
                        }

                        snapshot.children.count();
                    }else{
                        dataUser.clear()
                        messageViewModel.listMessage.value = dataUser;
                    }
                }
            }
        )

    }

    fun getUserInfo(userInfo: UserInfo) {
        firebase.userRef.child(userInfo.uid).addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                var user = snapshot.getValue(UserInfo::class.java);
                user?.chat = userInfo.chat;

                firebase.messagesRef.child(firebase.getID()).child(userInfo.uid)
                    .orderByChild("checkseen").equalTo(false)
                    .addValueEventListener(object : ValueEventListener {
                        override fun onCancelled(error: DatabaseError) {
                            TODO("Not yet implemented")
                        }

                        override fun onDataChange(snapshot: DataSnapshot) {
//                            var position = dataUser.indexOf(user!!)
                            user!!
                            var position = -1;
                            dataUser.forEachIndexed { index, it ->
                                if(it.uid == user.uid){
                                    position = index;
                                    return@forEachIndexed;
                                }
                            }
                            user.unreadMes = snapshot.children.count();
                            if (position != -1)
                                dataUser[position] = user
                            else
                                dataUser.add(user)
                            messageViewModel.listMessage.value = dataUser;
                        }

                    })
//                adapter.notifyDataSetChanged();
            }

        })
    }

    private fun init(view: View) {

        messageViewModel.listMessage.observe(viewLifecycleOwner, Observer {list ->

            listUser.clear()
            listUser.addAll(list)

            listUser.sortWith(kotlin.Comparator { t, t2 ->
                t2.chat.time.compareTo(t.chat.time)
            });

            adapter = RecyclerMessageAdapter(listUser, requireContext());
            val layoutManager = LinearLayoutManager(context);
            view.rc_message.layoutManager = layoutManager;
            view.loading.visibility = View.INVISIBLE

            view.rc_message.adapter = adapter;

            messageViewModel.search.observe(viewLifecycleOwner, Observer {key->
                listUser.clear();
                view.loading.visibility = View.VISIBLE

                list.forEach { user ->
                    if (user.name.toLowerCase().contains(key.trim().toLowerCase()))
                        listUser.add(user);
                }
                view.empty.isVisible = dataUser.size == 0;
                view.loading.visibility = View.INVISIBLE
                adapter.notifyDataSetChanged();
            })

            view.empty.isVisible = dataUser.size == 0;



        })

    }

    private fun handleEvent(view: View) {
        view.ic_createmess.setOnClickListener { v ->
            v.findNavController().navigate(R.id.action_homeFragment_to_createMessFragment);
        }
    }

}