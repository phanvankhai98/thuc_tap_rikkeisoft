package com.pvkhai.whatapp.view.Friend

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.pvkhai.whatapp.R
import com.pvkhai.whatapp.adapter.RecyclerAllAdapter
import com.pvkhai.whatapp.model.UserInfo
import com.pvkhai.whatapp.service.FirebaseService
import com.pvkhai.whatapp.viewmodel.FriendViewModel
import kotlinx.android.synthetic.main.fragment_all_friend.view.*

class AllFragment : Fragment() {

    lateinit var adapter: RecyclerAllAdapter;
    val firebase = FirebaseService();
    var data = ArrayList<UserInfo>();
    private val friendViewModel: FriendViewModel by activityViewModels();

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_all_friend, container, false);
        handleEvent(view);
        friendViewModel.listUser.observe(viewLifecycleOwner, Observer { listUser ->

            data.clear();
            data.addAll(listUser)

            adapter = RecyclerAllAdapter(data, requireContext());

            val layoutManager = LinearLayoutManager(context);
            view.rc_friend.layoutManager = layoutManager;
            view.rc_friend.adapter = adapter;

            view?.empty?.isVisible = data.size == 0;

            friendViewModel.search.observe(viewLifecycleOwner, Observer { key ->
                data.clear();
                listUser.forEach { user ->
                    if (user.name.toLowerCase().contains(key.trim().toLowerCase()))
                        data.add(user);
                }
                adapter.notifyDataSetChanged();
//                Toast.makeText(context,"oke",Toast.LENGTH_SHORT).show()
                view.empty.isVisible = data.size == 0;
            })
        })

        return view
    }

    private fun handleEvent(view: View?) {
        view?.swipeRefresh?.setOnRefreshListener {
            SwipeRefreshLayout.OnRefreshListener {
//                getAll();
            }
        }
    }


}