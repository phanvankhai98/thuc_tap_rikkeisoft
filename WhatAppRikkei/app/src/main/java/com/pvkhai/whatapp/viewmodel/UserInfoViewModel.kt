package com.pvkhai.whatapp.viewmodel

import android.media.Image
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pvkhai.whatapp.model.UserInfo

class UserInfoViewModel : ViewModel() {
    var fullName: MutableLiveData<String> = MutableLiveData();
    var phoneNumber: MutableLiveData<String> = MutableLiveData();
    var dateOfBirth: MutableLiveData<String> = MutableLiveData();
    var imageUser: MutableLiveData<String> = MutableLiveData();
    var imageCover: MutableLiveData<String> = MutableLiveData();
    var userInfo: MutableLiveData<UserInfo> = MutableLiveData();


//    var
}