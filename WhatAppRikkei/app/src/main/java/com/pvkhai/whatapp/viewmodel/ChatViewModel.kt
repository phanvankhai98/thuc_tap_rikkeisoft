package com.pvkhai.whatapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pvkhai.whatapp.model.Chat

class ChatViewModel: ViewModel() {
    var listChat =  MutableLiveData<ArrayList<Chat>>();
    var message = MutableLiveData<String>();
    var image: MutableLiveData<String> = MutableLiveData();
}