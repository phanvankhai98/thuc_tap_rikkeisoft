package com.pvkhai.whatapp.service

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference

class FirebaseService() {

//    var firebaseDB = FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    var mAuth: FirebaseAuth = FirebaseAuth.getInstance();
    var rootRef: DatabaseReference = FirebaseDatabase.getInstance().reference;
    var userImageProfileRef: StorageReference = FirebaseStorage.getInstance().reference.child("Profile Images");
    var imageFileRef: StorageReference = FirebaseStorage.getInstance().reference.child("Image Files");
    var userRef: DatabaseReference = FirebaseDatabase.getInstance().reference.child("Users");
    var chatRequestRef: DatabaseReference = FirebaseDatabase.getInstance().reference.child("Chat Requests");
    var contactsRef: DatabaseReference = FirebaseDatabase.getInstance().reference.child("Contacts");
    var messagesRef: DatabaseReference = FirebaseDatabase.getInstance().reference.child("Messages");

    init {
        rootRef.keepSynced(true);
    }

    fun getUser(): FirebaseUser? {
        return mAuth.currentUser;
    }

    fun getID(): String {
        return mAuth.currentUser?.uid.toString();
    }

}