package com.example.activitylifecycle

import android.content.Intent
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var media:MediaPlayer? = null;


    override fun onStart() {
        super.onStart()
        Log.i("LOG A","onStart");
    }

    override fun onResume() {
        super.onResume()
        Log.i("LOG A","onResume");

    }

    fun createMedia(){
        media = MediaPlayer.create(this,R.raw.bynd)
        media?.start()

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i("LOG A","onCreate");

        if (media == null)
            createMedia();
        else
            media?.start();
        setContentView(R.layout.activity_main)

            btnNext.setOnClickListener{
                Toast.makeText(this,"Oke click ",Toast.LENGTH_LONG).show();
                var intent = Intent(this,MainActivity2::class.java);
                startActivity(intent);
            }

    }


    override fun onPause() {
        super.onPause()
        Log.i("LOG A","onPause");
        media?.pause();
    }


    override fun onStop() {
        super.onStop()
        Log.i("LOG A","onStop");
        media?.pause();


    }


    override fun onDestroy() {
        super.onDestroy()
        Log.i("LOG A","onDestroy");
        media?.release();

    }
    override fun onRestart() {
        super.onRestart()
        Log.i("LOG A","onRestart");

        if (media == null)
            createMedia();
        else
            media?.start();
    }


}
