package com.example.activitylifecycle

import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main2.*

class MainActivity2 : AppCompatActivity() {

    var media:MediaPlayer? = null;

    override fun onStart() {
        super.onStart()
        Log.i("LOG B","onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.i("LOG B","onResume")

    }
    fun createMedia(){
        media = MediaPlayer.create(this,R.raw.hanquac)
        media?.start()

    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        Log.i("LOG B","onCreate")
        if(media == null)
        createMedia();

        btnBack.setOnClickListener {
            finish();
        }
    }

    override fun onPause() {
        super.onPause()
        Log.i("LOG B","onPause")
        media?.stop();
    }

    override fun onStop() {
        super.onStop()
        Log.i("LOG B","onStop")
        media?.stop()

    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("LOG B","onDestroy")
        media?.release();

    }

    override fun onRestart() {
        super.onRestart()
        Log.i("LOG B","onRestart")

    }
}