package com.example.fragmentlifecycle

import android.content.Context
import android.media.MediaPlayer
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [BlankFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class BlankFragment : Fragment() {

    var mediaPlayer: MediaPlayer? = null;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i("LOG FM1", "onCreate")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_blank, container, false)
        if (mediaPlayer == null) {
            mediaPlayer = MediaPlayer.create(context, R.raw.bynd);
            mediaPlayer?.start();
        } else {
            mediaPlayer?.start()
        }
        Log.i("LOG FM1", "onCreateView")
        return view;
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.i("LOG FM1", "onAttach")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Log.i("LOG FM1", "onActivityCreated")

    }

    override fun onStart() {
        super.onStart()
        Log.i("LOG FM1", "onStart")

    }

    override fun onResume() {
        super.onResume()
        Log.i("LOG FM1", "onResume")

    }

    override fun onPause() {
        super.onPause()
        Log.i("LOG FM1", "onPause")

    }

    override fun onStop() {
        super.onStop()
        mediaPlayer?.pause();
        Log.i("LOG FM1", "onStop")

    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.i("LOG FM1", "onDestroyView")


    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("LOG FM1", "onDestroy")

    }

    override fun onDetach() {
        super.onDetach()
        Log.i("LOG FM1", "onDetach")

    }
}