package com.example.fragmentlifecycle

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction();

        val blankFragment = BlankFragment()
        fragmentTransaction.add(R.id.frame,blankFragment);
        fragmentTransaction.commit();

        handleClick()
    }

    fun handleClick(){
        fragment1.setOnClickListener {
            val blankFragment2 = BlankFragment2()
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.frame, blankFragment2)
            transaction.addToBackStack(null)
            transaction.commit();

            var a = supportFragmentManager.backStackEntryCount
            Log.i("LOG", "$a")
        }
    }
}