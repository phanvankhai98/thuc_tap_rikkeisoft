package com.example.recyclerview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import kotlinx.android.synthetic.main.activity_stag.*

class StagActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stag)

        var data: ArrayList<Person> = ArrayList<Person>();

        data.add(Person("Phan Văn Khải",R.mipmap.img1));
        data.add(Person("Phan Văn Khải2",R.mipmap.img2));
        data.add(Person("Phan Văn Khải2",R.mipmap.img3));
        data.add(Person("Phan Văn Khải2",R.mipmap.img4));
        data.add(Person("Phan Văn Khải2",R.mipmap.img5));
        data.add(Person("Phan Văn Khải2",R.mipmap.img2));
        data.add(Person("Phan Văn Khải2",R.mipmap.img3));

        var stagAdapter = StagAdapter(data);

        rc_stag.adapter = stagAdapter;
        rc_stag.layoutManager = StaggeredGridLayoutManager(2,LinearLayoutManager.VERTICAL);

    }
}