package com.example.recyclerview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_grid.*

class GridActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grid)

        var data: ArrayList<Address> = ArrayList<Address>();

        data.add(Address("Phan Văn Khải","0123456789"));
        data.add(Address("Phan Văn Khải2","0123456789"));
        data.add(Address("Phan Văn Khải3","0123456789"));
        data.add(Address("Phan Văn Khải4","0123456789"));

        var recyclerAdapter = RecyclerAdapter(data);

        rc_grid.adapter = recyclerAdapter;
        rc_grid.layoutManager = GridLayoutManager(this,2);
    }
}