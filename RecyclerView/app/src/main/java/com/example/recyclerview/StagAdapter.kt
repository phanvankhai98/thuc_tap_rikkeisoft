package com.example.recyclerview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class StagAdapter(val data: ArrayList<Person>) : RecyclerView.Adapter<StagAdapter.PlaceHolder>(){

    class PlaceHolder (var view: View) : RecyclerView.ViewHolder(view){
        val image:ImageView = view.findViewById(R.id.image)
        val textView: TextView = view.findViewById(R.id.name)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaceHolder {

        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.rc_stag_item,parent,false);

        return PlaceHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: PlaceHolder, position: Int) {
        holder.image.setImageResource(data.get(position).photo);
        holder.textView.setText(data.get(position).name);
    }
}