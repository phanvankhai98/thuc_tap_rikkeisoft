package com.example.recyclerview

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnLinear.setOnClickListener {
            var intent: Intent = Intent(this,LinearActivity::class.java). also {startActivity(it)};
        }

        btnGrid.setOnClickListener {
            var intent:Intent = Intent(this,GridActivity::class.java). also { startActivity(it) };
        }

        btnStag.setOnClickListener {
            var intent:Intent = Intent(this, StagActivity::class.java). also {startActivity(it)};
        }
    }
}