package com.example.recyclerview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_linear.*

class LinearActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_linear)

        var data: ArrayList<Address> = ArrayList<Address>();

        data.add(Address("Phan Văn Khải","0123456789"));
        data.add(Address("Phan Văn Khải2","0123456789"));
        data.add(Address("Phan Văn Khải3","0123456789"));
        data.add(Address("Phan Văn Khải4","0123456789"));

        var recyclerAdapter = RecyclerAdapter(data);

        rc_linear.adapter = recyclerAdapter;
        rc_linear.layoutManager = LinearLayoutManager(this);
//        recycler.adapter = recyclerAdapter;
//        recycler.layoutManager = LinearLayoutManager(this);

    }
}