package com.example.recyclerview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class RecyclerAdapter (private val data:ArrayList<Address>): RecyclerView.Adapter<RecyclerAdapter.MyViewHolder>() {

    class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view){
        val tvSdt: TextView = view.findViewById(R.id.tv_sdt)
        val tvName: TextView = view.findViewById(R.id.tv_name);
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.recycler_view_item,parent,false);

        return MyViewHolder(view);
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.tvSdt.setText(data.get(position).sdt);
        holder.tvName.setText(data.get(position).name);
    }

}