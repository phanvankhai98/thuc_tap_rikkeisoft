package com.example.dialog

import android.app.DatePickerDialog
import android.app.Dialog
import android.app.ProgressDialog.show
import android.app.TimePickerDialog
import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.format.DateFormat.is24HourFormat
import android.view.LayoutInflater
import android.view.View
import android.widget.DatePicker
import android.widget.NumberPicker
import android.widget.TimePicker
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialog3_custom.*
import java.text.DateFormat
import java.util.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener {
            displayDialog1();
        }
        button2.setOnClickListener {
            displayDialog2();
        }
        button3.setOnClickListener {
            displayDialog3();
        }
        button4.setOnClickListener {
            displayDialog4();
        }
        button5.setOnClickListener {
            displayDialog5();
        }
        button6.setOnClickListener {
            displayDialog6();
        }
    }

    private fun displayDialog6() {
        DatePickerFragment().show(supportFragmentManager,"datePicker")
    }

    private fun displayDialog5() {
        TimePickerFragment().show(supportFragmentManager, "timePicker")
    }

    private fun displayDialog4() {
//        var view: View = LayoutInflater.from(this).inflate(R.layout.dialog3_custom,null);
//        val numPicker = view.findViewById(R.id.numPicker) as NumberPicker;
//        numPicker.minValue = 0;
//        numPicker.maxValue = 1000;
        var build: AlertDialog.Builder = AlertDialog.Builder(this);
        build.setTitle("Erase USB storge")
        build.setMessage("you'll lose all the photo in media")
//        build.setView(view);
        build.setCancelable(false);
        build.setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, which ->
        })
        build.setPositiveButton("Ok", DialogInterface.OnClickListener { dialog, which -> })
        build.show();
    }

    private fun displayDialog3() {
        var view: View = LayoutInflater.from(this).inflate(R.layout.dialog3_custom, null);
        val numPicker = view.findViewById(R.id.numPicker) as NumberPicker;
        numPicker.minValue = 0;
        numPicker.maxValue = 1000;
        var build: AlertDialog.Builder = AlertDialog.Builder(this);
        build.setTitle("Text message limit")
        build.setView(view);
        build.setCancelable(false);
        build.setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, which ->
        })
        build.setPositiveButton("Ok", DialogInterface.OnClickListener { dialog, which -> })
        build.show();
    }


    private fun displayDialog2() {
        var view: View = LayoutInflater.from(this).inflate(R.layout.dialog2_custom, null);

        var build: AlertDialog.Builder = AlertDialog.Builder(this);
        build.setTitle("Brightness")
        build.setView(view);
        build.setCancelable(false);
        build.setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, which ->
        })
        build.setPositiveButton("Ok", DialogInterface.OnClickListener { dialog, which -> })
        build.show();
    }

    fun displayDialog1() {
        var view: View = LayoutInflater.from(this).inflate(R.layout.dialog1_custom, null);

        var build: AlertDialog.Builder = AlertDialog.Builder(this);
        build.setTitle("Pick your toppic")
        build.setView(view);
        build.setCancelable(false);
        build.setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, which ->
        })
        build.setPositiveButton("Ok", DialogInterface.OnClickListener { dialog, which -> })
        build.show();
    }

    class TimePickerFragment : DialogFragment(), TimePickerDialog.OnTimeSetListener {

        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            // Use the current time as the default values for the picker
            val c = Calendar.getInstance()
            val hour = c.get(Calendar.HOUR_OF_DAY)
            val minute = c.get(Calendar.MINUTE)

            // Create a new instance of TimePickerDialog and return it
            return TimePickerDialog(activity, this, hour, minute, true)
        }

        override fun onTimeSet(view: TimePicker, hourOfDay: Int, minute: Int) {
            // Do something with the time chosen by the user
        }
    }

    class DatePickerFragment: DialogFragment(), DatePickerDialog.OnDateSetListener{

        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)

            return activity?.let { DatePickerDialog(it, this, year, month, day) }!!
        }

        override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {

        }

    }
}