package com.example.fragmentstatechange

import android.media.MediaPlayer
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [BlankFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class BlankFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    var mediaPlayer: MediaPlayer? = null;
    var position: Int? = null;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState != null && savedInstanceState.containsKey("media")) {
            position = savedInstanceState.getInt("media");
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        if (mediaPlayer == null) {
            mediaPlayer = MediaPlayer.create(context, R.raw.hanquac);
            mediaPlayer?.start();
        } else {
            position?.let { mediaPlayer?.seekTo(it) }
        }

        return inflater.inflate(R.layout.fragment_blank, container, false)

    }

//    override fun onSaveInstanceState(outState: Bundle) {
//        super.onSaveInstanceState(outState)
//
//        mediaPlayer?.currentPosition?. let {outState.putInt("media",it)}
//
//    }
//
//


}